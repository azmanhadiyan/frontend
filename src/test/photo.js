import React, { useState, useEffect } from "react";
import axios from "axios";

import { Row ,Card} from "react-bootstrap";

const PhotoList = () => {
  const [input, setInput] = useState({
    author: "",
    width: "",
    height: "",
    url: "",
    download_url: "",
    currentId: null,
  });
  const [photo, setPhoto] = useState([]);
  const [fetch, setFetch] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(
        `https://picsum.photos/v2/list?page=1&limit=100`
      );
      setPhoto(
        result.data.map((el) => {
          const { id, author, width, height, url, download_url } = el;
          return { id, author, width, height, url, download_url };
        })
      );
    };
    if (fetch) {
      fetchData();
      setFetch(false);
    }
  }, [fetch]);

  return (
    <>
      <div>
          <h2 style={{width:"85%", margin:"auto", textAlign:"left"}}> List Photo</h2>
        <hr></hr>
        {photo !== null && (
          <div style={{width: "85%", margin:"auto", marginTop:"30px"}}>
            <Row xs={1} md={4} className="g-4">
            {photo.map((item, index) => {
              return (
                <div key={index}>
                  <Card style={{ width: "18rem" }}>
                    <Card.Img variant="top" src={item.download_url} style={{width:"100%", height: "50vh"}} />
                    <Card.Body>
                      <Card.Title>{item.author}</Card.Title>
                      <Card.Text>{item.width}</Card.Text>
                      <Card.Text>{item.height}</Card.Text>
                    </Card.Body>
                  </Card>
                </div>
              );
            })}
                </Row>

          </div>
        )}
      </div>
    </>
  );
};
export default PhotoList;
