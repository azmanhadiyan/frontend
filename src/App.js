import './App.css';
import PhotoList from './test/photo';

import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <PhotoList/>
    </div>
  );
}

export default App;
